/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it

import './src/sass/styles.scss';
import 'bootstrap';
import "@fortawesome/fontawesome-pro/css/all.css";