import React, { Component } from 'react';
import Layout from '../components/Layout';
import SEO from '../components/seo';
import './index.scss';
import FirstPanel from '../components/Home/FirstPanel';
import SkillsPanel from '../components/Home/SkillsPanel';
import SocialMediaPanel from '../components/Home/SocialMediaPanel';

class IndexPage extends Component {
  render() {
    return (
      <Layout>
        <SEO title="Home" keywords={['javascript', 'php', 'engineer', 'developer']} />
        <FirstPanel />
        <SkillsPanel />
        <SocialMediaPanel />
      </Layout>
    );
  }
}

export default IndexPage;
