import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';

const DellamodaLogo = (props) => (
  <StaticQuery query={graphql`
    query {
      placeholderImage: file(relativePath: { eq: "dellamoda_logo.png" }) {
        childImageSharp {
          fluid(maxWidth: 500) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `}
  render={data => <Img fluid={data.placeholderImage.childImageSharp.fluid} />}
  />
);

export default DellamodaLogo;
