/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from 'react';
import PropTypes from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';
import Header from './Header';
import './Layout.scss';

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => [
        <Header key={0} siteTitle={data.site.siteMetadata.title} />,
        <main key={1}>{children}</main>
    ]}
  />
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
