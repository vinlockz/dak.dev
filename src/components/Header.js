import React, { Component } from 'react';
import { Link } from 'gatsby';
import { Waypoint } from 'react-waypoint';
import Logo from './images/Logo';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/pro-solid-svg-icons';
import classnames from 'classnames';
import './Header.scss';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAtTop: true,
    };
  }
  nav = () => {
    return (
      <nav className="navbar navbar-expand-lg">
        <Link className="navbar-brand" to="/">
          <Logo dark={true} scale={1} />
        </Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <FontAwesomeIcon icon={faBars} size="2x" className="bars" />
        </button>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item pr-4">
              <Link className="nav-link" to="/">Home</Link>
            </li>
            <li className="nav-item pr-4">
              <a className="nav-link" href="#">About Me</a>
            </li>
          </ul>
        </div>
      </nav>
    );
  };

  render() {
    const { isAtTop } = this.state;

    const headerClasses = classnames('pageHeader', {
      scrolled: !isAtTop,
    });

    return [
      <Waypoint
        onEnter={() => {
          this.setState({ isAtTop: true });
        }}
        onLeave={() => {
          this.setState({ isAtTop: false })
        }}
      />,
      <header className={headerClasses}>
        {this.nav()}
      </header>,
    ];
  }
}

export default Header;
