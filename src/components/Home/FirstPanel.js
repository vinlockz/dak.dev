import React, { Component } from 'react';
import './FirstPanel.scss';

class FirstPanel extends Component {
  render() {
    return (
      <article className="homeFirstPanelContainer">
        <section className="homeFirstPanel container-fluid">
          <div className="row h-100">
            <div className="col-12 col-lg-5 text-left offset-lg-1">
              <div className="about-content">
                <h1 className="font-weight-bold hero-title">Full-Stack Engineer & Gamer</h1>
                <p>Hello! My name is <b>Dak Washbrook</b> and I've been on computers since I was 5 years old!</p>
                <p>Computers have always been a great passion of mine that I inherited from my father. He introduced me to programming in Visual Basic at a very young age, around 5 years old. While that language did not stick with me, that, along with my avid usage of computers since then, gave me the groundwork to pursue my lifelong hobby and now career in development and engineering.</p>
                <p>The most important thing to me is that I am <b>always learning</b>.</p>
                {/*<p>I am a very self-motivated </p>*/}
                <br />
                <h3>
                  Find me on <span className="ml-3" />
                  <a rel="noreferrer" className="twitter" href="https://twitter.com/Vinlockz" target="_blank">
                    <i className="fab fa-twitter" />
                  </a>
                  <span className="ml-3 mr-3" />
                  <a rel="noreferrer" className="twitch" href="https://twitch.tv/vinlock" target="_blank">
                    <i className="fab fa-twitch" />
                  </a>
                </h3>
              </div>
            </div>
          </div>
        </section>
      </article>
    );
  }
}

export default FirstPanel;
