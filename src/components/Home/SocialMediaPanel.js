import React, { Component } from 'react';
import './SocialMediaPanel.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter, faTwitch, faBitbucket, faGithub } from '@fortawesome/free-brands-svg-icons';

class SocialMediaPanel extends Component {
  render() {
    return (
      <article className="socialMediaPanelContainer">
        <section className="socialMediaPanel container-fluid">
          <div className="row h-100">
            <div className="col-12 col-md-4 social-col one">
              <div className="one">
                <div className="social-panel">
                  <FontAwesomeIcon icon={faTwitter} className="panel-icon" />
                  <div className="social-data">
                    <p className="display-4">
                      <span className="font-weight-bolder">2023</span> <sup>followers</sup>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-2 social-col two">
              <div className="two">
                <div className="social-panel">
                  <FontAwesomeIcon icon={faBitbucket} className="panel-icon" />
                  <div className="social-data">
                    <p className="display-4">
                      <span className="font-weight-bolder">1000</span> <sup>commits</sup>
                    </p>
                    <p className="display-4">
                      <span className="font-weight-bolder">34</span> <sup>repositories</sup>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-2 social-col three">
              <div className="three">
                <div className="social-panel">
                  <FontAwesomeIcon icon={faGithub} className="panel-icon" />
                  <div className="social-data">
                    <p className="display-4">
                      <span className="font-weight-bolder">1000</span> <sup>commits</sup>
                    </p>
                    <p className="display-4">
                      <span className="font-weight-bolder">34</span> <sup>repositories</sup>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-4 social-col four">
              <div className="four">
                <div className="social-panel">
                  <FontAwesomeIcon icon={faTwitch} className="panel-icon" />
                  <div className="social-data">
                    <p className="display-4">
                      <span className="font-weight-bolder">585</span> <sup>followers</sup>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </article>
    );
  }
}

export default SocialMediaPanel;
